from django.db import models

# Create your models here.


class User(models.Model):
    user_id = models.CharField(primary_key=True, max_length=30)


class Task(models.Model):
    task_id = models.CharField(primary_key=True, max_length=30)
    launches = models.PositiveIntegerField()
    do_review = models.BooleanField()

    @property
    def price(self):
        __install_price = 2
        __launch_prince = 1
        __review_price = 4
        price = __install_price + __launch_prince*self.launches + __review_price*self.do_review
        return price


class Step(models.Model):
    TODO = 'TD'
    STARTED = 'ST'
    DONE = 'DN'
    STATUS_CHOICES = (
        (TODO, 'todo'),
        (STARTED, 'started'),
        (DONE, 'done'),
    )
    INSTALL = 'IN'
    LAUNCH = 'LN'
    REVIEW = 'RW'
    STEP_CHOICES = (
        (INSTALL, 'install'),
        (LAUNCH, 'launch'),
        (REVIEW, 'review')
    )
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    task_id = models.ForeignKey(Task, on_delete=models.CASCADE)
    day = models.PositiveIntegerField()
    status = models.CharField(max_length=2, choices=STATUS_CHOICES, default=TODO)
    step = models.CharField(max_length=2, choices=STEP_CHOICES)

    class Meta:
        ordering = ['day', 'step']



