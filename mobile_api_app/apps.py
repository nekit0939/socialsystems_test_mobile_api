from django.apps import AppConfig


class MobileApiAppConfig(AppConfig):
    name = 'mobile_api_app'
