from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse, HttpResponse
from mobile_api_app.models import Task, Step, User

# Create your views here.


def add_task_admin(request: HttpResponse):
    task_id = request.POST.get('task_id')
    launches = request.POST.get('launches')
    try:
        if int(launches) < 1:
            return JsonResponse(status=400, data={'error': 'Launches count must be greater then 1'})
        if Task.objects.filter(task_id=task_id).exists():
            return JsonResponse(status=400, data={'error': 'Task with such task_id already exists'})
        do_review = request.POST.get('do_review')
        Task.objects.create(task_id=task_id, launches=launches, do_review=do_review)
        return JsonResponse(status=200, data={'result': 'success'})
    except Exception:
        return JsonResponse(status=200, data={'result': 'failed', 'error': 'Bad parameter'})


def list_task_admin(request: HttpResponse):
    result = [{'task_id': i.task_id, 'launches': i.launches, 'do_review': i.do_review,
               'users_started': get_users_started_task(i.task_id), 'users_done': get_users_done_task(i.task_id)}
                for i in Task.objects.all()]
    return JsonResponse(status=200, data=result, safe=False)


def delete_task_admin(request: HttpResponse):
    task_id = request.POST.get('task_id')
    try:
        task = Task.objects.get(task_id=task_id)
        task.delete()
        return JsonResponse(status=200, data={'result': 'success'})
    except ObjectDoesNotExist:
        return JsonResponse(status=400, data={'error': 'Task with such task_id does not exist'})


def list_task_user(request: HttpResponse):
    user_id = request.POST.get('user_id')
    try:
        User.objects.get(user_id=user_id)
        result = [{'task_id': i.task_id, 'price': i.price,
                   'status': get_current_task_status(task_id=i.task_id, user_id=user_id)} for i in Task.objects.all()]
        return JsonResponse(status=200, data=result, safe=False)
    except ObjectDoesNotExist:
        return JsonResponse(status=400, data={'error': 'User with such user_id does not exist. Register please.'})


def complete_task_step_user(request: HttpResponse):
    user_id = request.POST.get('user_id')
    task_id = request.POST.get('task_id')
    try:
        user = User.objects.get(user_id=user_id)
    except ObjectDoesNotExist:
        return JsonResponse(status=400, data={'error': 'User with such user_id does not exist. Register please.'})
    try:
        task = Task.objects.get(task_id=task_id)
    except ObjectDoesNotExist:
        return JsonResponse(status=400, data={'error': 'Task with such task_id does not exist'})
    step = request.POST.get('step')
    todo_step = Step.objects.filter(task_id=task, user_id=user, status=Step.TODO).first()
    steps_count = Step.objects.filter(task_id=task, user_id=user).count()
    steps_done_count = Step.objects.filter(task_id=task, user_id=user, status=Step.DONE).count()
    if not todo_step:
        if steps_count == steps_done_count and steps_count > 0:
            return JsonResponse(status=400, data={'error': 'You have done this task'})
        else:
            todo_step = init_steps(task, user)
    if todo_step.get_step_display() != step:
        return JsonResponse(status=400, data={'error': 'You are trying do the wrong step'})
    else:
        todo_step.status = Step.DONE
        todo_step.save()
        return JsonResponse(status=200, data={'result': 'success'})


def show_task_step_user(request: HttpResponse):
    user_id = request.POST.get('user_id')
    task_id = request.POST.get('task_id')
    try:
        user = User.objects.get(user_id=user_id)
    except ObjectDoesNotExist:
        return JsonResponse(status=400, data={'error': 'User with such user_id does not exist. Register please.'})
    try:
        task = Task.objects.get(task_id=task_id)
    except ObjectDoesNotExist:
        return JsonResponse(status=400, data={'error': 'Task with such task_id does not exist'})
    steps = Step.objects.filter(user_id=user, task_id=task)
    days = set([i.day for i in steps])
    result = []
    for day in days:
        res = {'day': day}
        day_steps = steps.filter(day=day)
        res['steps'] = [{
            'step': i.get_step_display(),
            'status': i.get_status_display()
        } for i in day_steps]
        result.append(res)
    return JsonResponse(status=200, data=result, safe=False)


def get_users_started_task(task_id):
    """
    :param task_id: id of Task
    :return: List of Users (user_ids) who started task whith task_id
    """
    user_list = [user.user_id for user in User.objects.all()
                 if get_current_task_status(task_id, user.user_id) == 'started']
    return user_list


def get_users_done_task(task_id):
    """
    :param task_id: id of Task
    :return: List of Users (user_ids) who done requested task
    """
    user_list = [user.user_id for user in User.objects.all()
                 if get_current_task_status(task_id, user.user_id) == 'done']
    return user_list


def init_steps(task, user):
    """
    :param task: task object
    :param user: user object
    :return: first step for requested user and task
    """
    install_step = Step.objects.create(task_id=task, user_id=user, step=Step.INSTALL, day=1)
    for i in range(task.launches):
        Step.objects.create(task_id=task, user_id=user, step=Step.LAUNCH, day=i + 1)
    if task.do_review:
        if task.launches < 7:
            Step.objects.create(task_id=task, user_id=user, step=Step.LAUNCH, day=7)
        Step.objects.create(task_id=task, user_id=user, step=Step.REVIEW, day=7)
    return install_step


def get_current_task_status(task_id, user_id):
    """
    :param task_id:
    :param user_id:
    :return: return status of task for reqested task and user
    """
    steps_count = Step.objects.filter(task_id=task_id, user_id=user_id).count()
    steps_done_count = Step.objects.filter(task_id=task_id, user_id=user_id, status=Step.DONE).count()
    if not steps_done_count:
        return 'todo'
    elif steps_done_count < steps_count:
        return 'started'
    elif steps_done_count == steps_count:
        return 'done'











