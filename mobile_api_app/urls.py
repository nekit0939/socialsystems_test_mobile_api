from django.conf.urls import url
from . import views
from django.urls import path

urlpatterns = [
    path('admin/add', views.add_task_admin, name='add_task_adm'),
    path('admin/list', views.list_task_admin, name='list_task_adm'),
    path('admin/delete', views.delete_task_admin, name='delete_task_adm'),
    path('user/list', views.list_task_user, name='list_task_usr'),
    path('user/do', views.complete_task_step_user, name='complete_task_step_usr'),
    path('user/task', views.show_task_step_user, name='show_task_step_usr'),
]
